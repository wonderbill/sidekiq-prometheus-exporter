#!/bin/bash -e
# This script is intended to be used to push the latest sidekiq-metrics 
# (i.e., sidekiq-prometheus-exporter)image to our ECR repository.
# To execute this script, you must have AWS credentials already configured,
# either via an AWS profile or via the environment variables AWS_ACCESS_KEY_ID,
# AWS_SECRET_ACCESS_KEY, and AWS_DEFAULT_REGION.
# This script must be executed from within the /docker directory of this git project.

export SERVICE_NAME="sidekiq-metrics" # the name of the image repository in our ECR
REGISTRY_URL="908367246927.dkr.ecr.eu-west-1.amazonaws.com"
export AWS_REGION="eu-west-1" # the region in which our ECR repository lives
export LAST_COMMIT=`git log -1 ../ | awk 'NR==1 {print $2}'` # used as the image tag
echo "The latest commit for ${SERVICE_NAME} is: ${LAST_COMMIT}"

# Login to ECR
$(aws ecr get-login --no-include-email --region $AWS_REGION)

# Only push if the image has been changed since the last push
set +e # this allows the following command to fail
IMAGE_META=`aws ecr describe-images --repository-name=${SERVICE_NAME} --image-ids=imageTag=$LAST_COMMIT --region $AWS_REGION 2> /dev/null`

if [ $? == 0 ]; then
  echo "${SERVICE_NAME} is unchanged. There is no need to push a new image."
else
  set -e
  echo "${SERVICE_NAME} has changed. Building and pushing new image to ECR."
  docker build . -t "${REGISTRY_URL}/${SERVICE_NAME}:${LAST_COMMIT}"
  docker push "${REGISTRY_URL}/${SERVICE_NAME}:${LAST_COMMIT}"
  echo "\nImage tag/version: ${LAST_COMMIT}"
fi
